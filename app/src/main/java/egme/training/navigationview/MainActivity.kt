package egme.training.navigationview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.navigation.NavigationView
import egme.training.navigationview.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.content_main.view.*

class MainActivity : AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener {
    private lateinit var MainActivityBinding:ActivityMainBinding
    //lateinit var toolbar: Toolbar
    //lateinit var drawerLayout: DrawerLayout
    //lateinit var navView: NavigationView
    override fun onCreate(savedInstanceState: Bundle?)  {
        super.onCreate(savedInstanceState)
        MainActivityBinding=DataBindingUtil.setContentView(this,R.layout.activity_main)
        //setContentView(R.layout.activity_main)

        //toolbar = MainActivityBinding.Drawer.toolbar//findViewById(R.id.toolbar)
        setSupportActionBar(MainActivityBinding.Drawer.toolbar)

        //drawerLayout = MainActivityBinding.Drawer //findViewById(R.id.Drawer)

        //navView = MainActivityBinding.navView  //findViewById(R.id.nav_view)
        MainActivityBinding.ContentMain.MyText.text="Testing Data Binding"

        val toggle = ActionBarDrawerToggle(
            this, MainActivityBinding.Drawer, MainActivityBinding.ContentMain.toolbar, 0, 0)

        MainActivityBinding.Drawer.addDrawerListener(toggle)
        toggle.syncState()
        MainActivityBinding.navView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_profile -> {
                Toast.makeText(this, "Profile clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_messages -> {
                Toast.makeText(this, "Messages clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_friends -> {
                Toast.makeText(this, "Friends clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_update -> {
                Toast.makeText(this, "Update clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_logout -> {
                Toast.makeText(this, "Sign out clicked", Toast.LENGTH_SHORT).show()
            }
        }
        MainActivityBinding.Drawer.closeDrawer(GravityCompat.START)
        return true
    }
}
